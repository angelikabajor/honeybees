package com.gitlab.angelikabajor

import models.Pollen

package object measurements {

  private[measurements] def findPollen(pollens: Seq[Pollen], pollenId: Int) = {
    pollens.find(pollen => pollen.id == pollenId)
  }

  private[measurements] def filterKeysWithValueMatching[A, B](inputMap: Map[A, B], value: B) = {
    inputMap
      .filter(_._2 == value)
//      .map { case (key, _) => key }
      .toSeq
  }

}
