package com.gitlab.angelikabajor
package measurements

import models.{Harvest, Pollen}

object DayMeasurements {

  def getSugarPerDay(pollens: Seq[Pollen], harvest: Seq[Harvest]): Map[String, Double] = {
    harvest
      .map(h => (h.day, h.miligramsHarvested, findPollen(pollens, h.pollenId)))
      .filter(_._3.isDefined)
      .map { case (day, mg, pollen) => (day, mg * pollen.get.sugarPerMg) }
      .groupMapReduce(_._1)(_._2)(_ + _)
  }

  def getMostEfficientDays(sugarByDay: Map[String, Double]): Seq[(String, Double)] = {
    if (sugarByDay.isEmpty) return Seq.empty
    filterKeysWithValueMatching(sugarByDay, sugarByDay.values.max)
  }

  def getLeastEfficientDays(sugarByDay: Map[String, Double]): Seq[(String, Double)] = {
    if (sugarByDay.isEmpty) return Seq.empty
    filterKeysWithValueMatching(sugarByDay, sugarByDay.values.min)
  }

}
