package com.gitlab.angelikabajor
package measurements

import models.{Harvest, Pollen}

object BeeMeasurements {

  def getBeesEffectiveness(pollens: Seq[Pollen], harvest: Seq[Harvest]): Map[Int, Double] = {
    harvest
      .map(h => (h.beeId, h.miligramsHarvested, findPollen(pollens, h.pollenId)))
      .filter(_._3.isDefined)
      .map { case (beeId, mg, pollen) => (beeId, mg * pollen.get.sugarPerMg) }
      .groupMap(_._1)(_._2)
      .map { case (beeId, dailySugar) => beeId -> dailySugar.sum / dailySugar.size }
  }

  def getMostEffectiveBees(beesEffectiveness: Map[Int, Double]): Seq[(Int, Double)] = {
    if (beesEffectiveness.isEmpty) return Seq.empty
    filterKeysWithValueMatching(beesEffectiveness, beesEffectiveness.values.max)
  }

  def getLeastEffectiveBees(beesEffectiveness: Map[Int, Double]): Seq[(Int, Double)] = {
    if (beesEffectiveness.isEmpty) return Seq.empty
    filterKeysWithValueMatching(beesEffectiveness, beesEffectiveness.values.min)
  }

}
