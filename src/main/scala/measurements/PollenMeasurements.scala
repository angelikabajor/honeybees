package com.gitlab.angelikabajor
package measurements

import models.{Harvest, Pollen}

object PollenMeasurements {

  def getMostContributedPollen(pollens: Seq[Pollen], harvest: Seq[Harvest]): Seq[(Pollen, Double)] = {
    val totalSugarByPollen = getTotalSugarByPollen(pollens, harvest)
    if (totalSugarByPollen.isEmpty) return Seq.empty

    filterKeysWithValueMatching(totalSugarByPollen, totalSugarByPollen.values.max)
  }

  def getMostPopularPollen(pollens: Seq[Pollen], harvest: Seq[Harvest]): Seq[(Pollen, Int)] = {
    val popularityPerPollen = getPopularityPerPollen(pollens, harvest)
    if (popularityPerPollen.isEmpty) return Seq.empty

    filterKeysWithValueMatching(popularityPerPollen, popularityPerPollen.values.max)
  }

  private def getTotalSugarByPollen(pollens: Seq[Pollen], harvest: Seq[Harvest]) = {
    harvest
      .map(harv => (findPollen(pollens, harv.pollenId), harv.miligramsHarvested))
      .filter(_._1.isDefined)
      .map { case (pollen, miligrams) => (pollen.get, miligrams) }
      .map { case (pollen, miligrams) => (pollen, miligrams * pollen.sugarPerMg) }
      .groupMapReduce(_._1)(_._2)(_ + _)
  }

  private def getPopularityPerPollen[A](pollens: Seq[Pollen], harvest: Seq[Harvest]) = {
    harvest
      .map(_.pollenId)
      .groupBy(identity)
      .map { case (pollenId, occurrences) => (findPollen(pollens, pollenId), occurrences.size) }
      .filter(_._1.isDefined)
      .map { case (pollen, popularity) => (pollen.get, popularity) }
  }
}
