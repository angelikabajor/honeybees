package com.gitlab.angelikabajor
package models

case class Harvest(beeId: Int, day: String, pollenId: Int, miligramsHarvested: Double)
