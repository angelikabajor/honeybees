package com.gitlab.angelikabajor
package models

case class Pollen(id: Int, name: String, sugarPerMg: Double)
