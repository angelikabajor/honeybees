package com.gitlab.angelikabajor
package reader

import scala.io.Source

class CsvReader extends Reader {

  private val csvColumnDelimiter = ','

  def readEntitiesAsArrays(filename: String, skipHeader: Boolean = false): Seq[Array[String]] = {
    val lines = Source.fromResource(filename).getLines()
    if (skipHeader) lines.next()

    lines
      .map(p => p.split(csvColumnDelimiter).map(_.trim))
      .toSeq
  }
}
