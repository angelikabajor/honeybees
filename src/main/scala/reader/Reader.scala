package com.gitlab.angelikabajor
package reader

trait Reader {

  def readEntitiesAsArrays(filename: String, skipHeader: Boolean = false): Seq[Array[String]]
}
