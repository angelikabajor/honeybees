package com.gitlab.angelikabajor

import measurements.{BeeMeasurements, DayMeasurements, PollenMeasurements}
import models.{Harvest, Pollen}
import reader.{CsvReader, Reader}

object Honeybees extends App {

  private val pollensFilename = "pollens.csv"
  private val harvestFilename = "harvest.csv"
  private val reader: Reader = new CsvReader

  private val pollens = reader
    .readEntitiesAsArrays(pollensFilename, skipHeader = true)
    .map(p => Pollen(p(0).toInt, p(1), p(2).toDouble))
  private val harvest = reader
    .readEntitiesAsArrays(harvestFilename, skipHeader = true)
    .map(p => Harvest(p(0).toInt, p(1), p(2).toInt, p(3).toDouble))

  private val mostContributedPollen: Seq[(Pollen, Double)] = PollenMeasurements.getMostContributedPollen(pollens, harvest)
  private val mostPopularPollen: Seq[(Pollen, Int)] = PollenMeasurements.getMostPopularPollen(pollens, harvest)
  private val sugarByDay: Map[String, Double] = DayMeasurements.getSugarPerDay(pollens, harvest)
  private val mostEfficientDays: Seq[(String, Double)] = DayMeasurements.getMostEfficientDays(sugarByDay)
  private val leastEfficientDays: Seq[(String, Double)] = DayMeasurements.getLeastEfficientDays(sugarByDay)
  private val beesEffectiveness: Map[Int, Double] = BeeMeasurements.getBeesEffectiveness(pollens, harvest)
  private val mostEffectiveBees: Seq[(Int, Double)] = BeeMeasurements.getMostEffectiveBees(beesEffectiveness)
  private val leastEffectiveBees: Seq[(Int, Double)] = BeeMeasurements.getLeastEffectiveBees(beesEffectiveness)

  println("1. Which pollen contributed the most sugar in total?\n\t" + mostContributedPollen + "\n")

  println("2. Which pollen was most popular among the bees?\n\t" + mostPopularPollen + "\n")

  println("3. Which day was the best day for harvesting? Which one was the worst? (A chart or table that shows total sugar per day would be great."
    + "\n\tMost efficient day: " + mostEfficientDays
    + "\n\tLeast efficient day: " + leastEfficientDays
    + "\n\tSugar by day: \n" + formatMapToTable(sugarByDay, Ordering.String) + "\n")

  println("4. Which bee was most effective (**udarnik**)? Which one was the least? Effectiveness is measured as average sugar harvested per each working day. Getting a table with each bee relative effectiveness is a plus."
    + "\n\tMost effective bee: " + mostEffectiveBees
    + "\n\tLeast effective bee: " + leastEffectiveBees
    + "\n\tBees effectiveness (beeId, averageDailySugar): \n" + formatMapToTable(beesEffectiveness, Ordering.Int) + "\n")

  private def formatMapToTable[A](inputMap: Map[A, Double], ordering: Ordering[A]) = {
    inputMap.keys.toSeq.sorted(ordering)
      .map(key => s"\t\t$key\t" + f"${inputMap(key)}%1.1f")
      .mkString("\n")
  }
}
