package com.gitlab.angelikabajor
package reader

import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers._

import java.io.FileNotFoundException

class CsvReaderTest extends AnyFunSpec {

  val reader: Reader = new CsvReader

  describe("Reading entries as arrays") {

    it("should read entries as arrays from csv with header") {
      val filename = "pollens_header.csv"
      val entitiesAsArrays = reader.readEntitiesAsArrays(filename, skipHeader = true)
      entitiesAsArrays should contain only(Array("1", "Canola", "10"), Array("2", "Bluebell", "3"))
    }

    it("should read entries as arrays from csv without header") {
      val filename = "pollens_no_header.csv"
      val entitiesAsArrays = reader.readEntitiesAsArrays(filename)
      entitiesAsArrays should contain only(Array("1", "Canola", "10"), Array("2", "Bluebell", "3"))
    }

    it("should return empty sequence when file is empty") {
      val filename = "pollens_empty.csv"
      val entitiesAsArrays = reader.readEntitiesAsArrays(filename)
      entitiesAsArrays should be(empty)
    }

    it("should throw exception when file do not exist") {
      val filename = "not_existing_file.csv"
      an[FileNotFoundException] should be thrownBy reader.readEntitiesAsArrays(filename)
    }
  }
}
