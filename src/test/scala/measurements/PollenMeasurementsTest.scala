package com.gitlab.angelikabajor
package measurements

import models.{Harvest, Pollen}

import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers._

class PollenMeasurementsTest extends AnyFunSpec {

  val day1 = "2013-04-01"
  val day2 = "2013-04-02"
  val pollensWithIds01: Seq[Pollen] = Seq(Pollen(0, "Bluebell", 1), Pollen(1, "Rhododendron", 1))
  val pollensWithIds012: Seq[Pollen] = pollensWithIds01 :+ Pollen(2, "Thyme", 1)

  describe("Most contributed pollen") {
    val harvestForPollens01 = Seq(
      Harvest(10, day1, pollenId = 0, miligramsHarvested = 1),
      Harvest(11, day2, pollenId = 0, miligramsHarvested = 1),
      Harvest(12, day1, pollenId = 1, miligramsHarvested = 5),
      Harvest(13, day2, pollenId = 1, miligramsHarvested = 5))
    val harvestForPollens012 = harvestForPollens01 ++ Seq(
      Harvest(14, day1, pollenId = 2, miligramsHarvested = 5),
      Harvest(15, day2, pollenId = 2, miligramsHarvested = 5))

    it("should return most contributed pollen") {
      val mostContributedPollen = PollenMeasurements.getMostContributedPollen(pollensWithIds01, harvestForPollens01)
      mostContributedPollen should contain only((pollensWithIds01(1), 10.0))
    }

    it("should return multiple most contributed pollens") {
      val mostContributedPollen = PollenMeasurements.getMostContributedPollen(pollensWithIds012, harvestForPollens012)
      mostContributedPollen should contain only((pollensWithIds012(1), 10.0), (pollensWithIds012(2), 10.0))
    }

    it("should return empty list when harvest list is empty") {
      val harvest = Seq.empty
      val mostContributedPollen = PollenMeasurements.getMostContributedPollen(pollensWithIds01, harvest)
      mostContributedPollen should be(empty)
    }

    it("should return empty list when pollens list is empty") {
      val pollens = Seq.empty
      val mostContributedPollen = PollenMeasurements.getMostContributedPollen(pollens, harvestForPollens01)
      mostContributedPollen should be(empty)
    }

    it("should return most contributed pollen when some pollen id is missing") {
      val mostContributedPollen = PollenMeasurements.getMostContributedPollen(pollensWithIds01, harvestForPollens012)

      mostContributedPollen should contain only((pollensWithIds01(1), 10.0))
    }
  }

  describe("Most popular pollen") {

    val harvestForPollens01 = Seq(
      Harvest(10, day1, pollenId = 0, miligramsHarvested = 5),
      Harvest(12, day1, pollenId = 1, miligramsHarvested = 1),
      Harvest(13, day2, pollenId = 1, miligramsHarvested = 1))
    val harvestForPollens012 = harvestForPollens01 ++ Seq(
      Harvest(14, day1, pollenId = 2, miligramsHarvested = 2),
      Harvest(15, day2, pollenId = 2, miligramsHarvested = 2))

    it("should return most popular pollen") {
      val mostPopularPollen = PollenMeasurements.getMostPopularPollen(pollensWithIds01, harvestForPollens01)
      mostPopularPollen should contain only((pollensWithIds01(1), 2))
    }

    it("should return multiple most popular pollens") {
      val mostPopularPollen = PollenMeasurements.getMostPopularPollen(pollensWithIds012, harvestForPollens012)
      mostPopularPollen should contain only((pollensWithIds012(1), 2), (pollensWithIds012(2), 2))
    }

    it("should return empty list when harvest list is empty") {
      val harvest = Seq.empty
      val mostPopularPollen = PollenMeasurements.getMostPopularPollen(pollensWithIds01, harvest)
      mostPopularPollen should be(empty)
    }

    it("should return empty list when pollens list is empty") {
      val pollens = Seq.empty
      val mostPopularPollen = PollenMeasurements.getMostPopularPollen(pollens, harvestForPollens01)
      mostPopularPollen should be(empty)
    }

    it("should return most popular pollen when some pollen id is missing") {
      val mostPopularPollen = PollenMeasurements.getMostPopularPollen(pollensWithIds01, harvestForPollens012)

      mostPopularPollen should contain only((pollensWithIds01(1), 2))
    }
  }


}
