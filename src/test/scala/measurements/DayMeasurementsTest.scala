package com.gitlab.angelikabajor
package measurements

import models.{Harvest, Pollen}

import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers._

class DayMeasurementsTest extends AnyFunSpec {

  val day1 = "2013-04-01"
  val day2 = "2013-04-02"
  val day3 = "2013-04-03"
  val day4 = "2013-04-04"
  val pollensWithIds01: Seq[Pollen] = Seq(Pollen(0, "Bluebell", 1), Pollen(1, "Rhododendron", 1))
  val pollensWithIds012: Seq[Pollen] = pollensWithIds01 :+ Pollen(2, "Thyme", 1)
  val harvestForDays12: Seq[Harvest] = Seq(
    Harvest(5, day1, pollenId = 0, miligramsHarvested = 1),
    Harvest(6, day1, pollenId = 1, miligramsHarvested = 4),
    Harvest(5, day2, pollenId = 0, miligramsHarvested = 3),
    Harvest(6, day2, pollenId = 1, miligramsHarvested = 6))
  val harvestForPollens012AndUnknown: Seq[Harvest] = harvestForDays12 ++ Seq(
    Harvest(5, day3, pollenId = 2, miligramsHarvested = 100),
    Harvest(6, day3, pollenId = 2, miligramsHarvested = 100))
  val harvestForDays1234: Seq[Harvest] = harvestForDays12 ++ Seq(
    Harvest(5, day3, pollenId = 0, miligramsHarvested = 1),
    Harvest(6, day3, pollenId = 1, miligramsHarvested = 4),
    Harvest(5, day4, pollenId = 0, miligramsHarvested = 3),
    Harvest(6, day4, pollenId = 1, miligramsHarvested = 6))

  describe("Daily sugar") {

    it("should return sugar per day") {
      val sugarByDay = DayMeasurements.getSugarPerDay(pollensWithIds01, harvestForDays1234)
      sugarByDay should contain only(day1 -> 5.0, day2 -> 9.0, day3 -> 5.0, day4 -> 9.0)
    }

    it("should return empty list when harvest list is empty") {
      val harvest = Seq.empty
      val sugarByDay = DayMeasurements.getSugarPerDay(pollensWithIds01, harvest)
      sugarByDay should be(empty)
    }

    it("should return empty list when pollens list is empty") {
      val pollens = Seq.empty
      val sugarByDay = DayMeasurements.getSugarPerDay(pollens, harvestForDays12)
      sugarByDay should be(empty)
    }

    it("should return sugar per day ignoring unknown pollenId") {
      val sugarByDay = DayMeasurements.getSugarPerDay(pollensWithIds01, harvestForPollens012AndUnknown)
      sugarByDay should contain only(day1 -> 5.0, day2 -> 9.0)
    }
  }

  describe("Most efficient days") {

    it("should return most efficient days") {
      val mostEfficientDays = DayMeasurements.getMostEfficientDays(DayMeasurements.getSugarPerDay(pollensWithIds01, harvestForDays12))
      mostEfficientDays should contain only ((day2, 9.0))
    }

    it("should return multiple most efficient days") {
      val mostEfficientDays = DayMeasurements.getMostEfficientDays(DayMeasurements.getSugarPerDay(pollensWithIds01, harvestForDays1234))
      mostEfficientDays should contain only((day2, 9.0), (day4, 9.0))
    }

    it("should return empty list when sugar by day is empty") {
      val sugarByDay: Map[String, Double] = Map.empty
      val mostEfficientDays = DayMeasurements.getMostEfficientDays(sugarByDay)
      mostEfficientDays should be(empty)
    }
  }

  describe("Least efficient days") {

    it("should return least efficient days") {
      val leastEfficientDays = DayMeasurements.getLeastEfficientDays(DayMeasurements.getSugarPerDay(pollensWithIds01, harvestForDays12))
      leastEfficientDays should contain only ((day1, 5.0))
    }

    it("should return multiple least efficient days") {
      val leastEfficientDays = DayMeasurements.getLeastEfficientDays(DayMeasurements.getSugarPerDay(pollensWithIds01, harvestForDays1234))
      leastEfficientDays should contain only((day1, 5.0), (day3, 5.0))
    }

    it("should return empty list when sugar by day is empty") {
      val sugarByDay: Map[String, Double] = Map.empty
      val leastEfficientDays = DayMeasurements.getLeastEfficientDays(sugarByDay)
      leastEfficientDays should be(empty)
    }
  }
}
