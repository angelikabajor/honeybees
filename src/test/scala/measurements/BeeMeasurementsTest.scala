package com.gitlab.angelikabajor
package measurements

import models.{Harvest, Pollen}

import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers._

class BeeMeasurementsTest extends AnyFunSpec {

  val day1 = "2013-04-01"
  val day2 = "2013-04-02"
  val day3 = "2013-04-03"
  val pollensWithIds01: Seq[Pollen] = Seq(Pollen(0, "Bluebell", 1), Pollen(1, "Rhododendron", 1))
  val pollensWithIds012: Seq[Pollen] = pollensWithIds01 :+ Pollen(2, "Thyme", 1)

  describe("Bees effectiveness") {
    val harvestForPollens01 = Seq(
      Harvest(beeId = 5, day1, pollenId = 0, miligramsHarvested = 1),
      Harvest(beeId = 5, day2, pollenId = 1, miligramsHarvested = 3),
      Harvest(beeId = 6, day1, pollenId = 0, miligramsHarvested = 4),
      Harvest(beeId = 6, day2, pollenId = 1, miligramsHarvested = 6))
    val harvestForPollens012 = harvestForPollens01 ++ Seq(
      Harvest(beeId = 5, day3, pollenId = 2, miligramsHarvested = 5),
      Harvest(beeId = 6, day3, pollenId = 2, miligramsHarvested = 8))

    it("should return effectiveness per bee") {
      val beesEffectiveness = BeeMeasurements.getBeesEffectiveness(pollensWithIds01, harvestForPollens01)
      beesEffectiveness should contain only(5 -> 2.0, 6 -> 5.0)
    }

    it("should return empty list when harvest list is empty") {
      val harvest = Seq.empty
      val beesEffectiveness = BeeMeasurements.getBeesEffectiveness(pollensWithIds01, harvest)
      beesEffectiveness should be(empty)
    }

    it("should return empty list when pollens list is empty") {
      val pollens = Seq.empty
      val beesEffectiveness = BeeMeasurements.getBeesEffectiveness(pollens, harvestForPollens01)
      beesEffectiveness should be(empty)
    }

    it("should return effectiveness per bee ignoring unknown pollenId") {
      val beesEffectiveness = BeeMeasurements.getBeesEffectiveness(pollensWithIds01, harvestForPollens012)

      beesEffectiveness should contain only(5 -> 2.0, 6 -> 5.0)
    }
  }

  describe("Most effective bees") {
    val harvestForBees56 = Seq(
      Harvest(beeId = 5, day1, pollenId = 0, miligramsHarvested = 1),
      Harvest(beeId = 5, day2, pollenId = 1, miligramsHarvested = 3),
      Harvest(beeId = 6, day1, pollenId = 0, miligramsHarvested = 4),
      Harvest(beeId = 6, day2, pollenId = 1, miligramsHarvested = 6))
    val harvestForBees567 = harvestForBees56 ++ Seq(
      Harvest(beeId = 7, day1, pollenId = 0, miligramsHarvested = 4),
      Harvest(beeId = 7, day2, pollenId = 1, miligramsHarvested = 6))

    it("should return most effective bees") {
      val mostEffectiveBees = BeeMeasurements.getMostEffectiveBees(BeeMeasurements.getBeesEffectiveness(pollensWithIds01, harvestForBees56))
      mostEffectiveBees should contain only ((6, 5.0))
    }

    it("should return multiple most effective bees") {
      val mostEffectiveBees = BeeMeasurements.getMostEffectiveBees(BeeMeasurements.getBeesEffectiveness(pollensWithIds01, harvestForBees567))
      mostEffectiveBees should contain only ((6, 5.0), (7, 5.0))
    }

    it("should return empty list when bees effectiveness is empty") {
      val beesEffectiveness: Map[Int, Double] = Map.empty

      val mostEffectiveBees = BeeMeasurements.getMostEffectiveBees(beesEffectiveness)
      mostEffectiveBees should be(empty)
    }
  }

  describe("Least effective bees") {
    val harvestForBees56 = Seq(
      Harvest(beeId = 5, day1, pollenId = 0, miligramsHarvested = 1),
      Harvest(beeId = 5, day2, pollenId = 1, miligramsHarvested = 3),
      Harvest(beeId = 6, day1, pollenId = 0, miligramsHarvested = 4),
      Harvest(beeId = 6, day2, pollenId = 1, miligramsHarvested = 6))
    val harvestForBees567 = harvestForBees56 ++ Seq(
      Harvest(beeId = 7, day1, pollenId = 0, miligramsHarvested = 1),
      Harvest(beeId = 7, day2, pollenId = 1, miligramsHarvested = 3))

    it("should return least effective bees") {
      val leastEffectiveBees = BeeMeasurements.getLeastEffectiveBees(BeeMeasurements.getBeesEffectiveness(pollensWithIds01, harvestForBees56))
      leastEffectiveBees should contain only ((5, 2.0))
    }

    it("should return multiple least effective bees") {
      val leastEffectiveBees = BeeMeasurements.getLeastEffectiveBees(BeeMeasurements.getBeesEffectiveness(pollensWithIds01, harvestForBees567))
      leastEffectiveBees should contain only((5, 2.0), (7, 2.0))
    }

    it("should return empty list when bees effectiveness is empty") {
      val beesEffectiveness: Map[Int, Double] = Map.empty

      val leastEffectiveBees = BeeMeasurements.getLeastEffectiveBees(beesEffectiveness)
      leastEffectiveBees should be(empty)
    }
  }
}
