# Honeybees need your help!
A savvy swarm lived peacufully in some undisclosed location. One day, the queen said:

> _I'm fed up with this BS. You can't even tell me which pollen is the most effective for our swarm, or who is our **udarnik**!_

This made the bees sad (you can imagine [Der Untergang](http://en.wikipedia.org/wiki/Downfall_(2004_film)) final scene here)... However, not all was lost! One smart bee came forward and said:

> _Oh c'mon, it's no big deal. We've been tracking each of our bees and collected some really good data. I'm sure it could be used for the swarm analytics._

# Data

There are two CSV files.

### pollen.csv

It contains information about each pollen effectivenes - i.e. how much of the sugar can be extracted from each mg of pollen.

### harvest.csv

This file contains information about harvest done by each of the bees each day. I.e. how much of what pollen was harvested on a given day.

# Task

Help the queen with finding out a couple of answers:

1. Which pollen contributed the most sugar in total?
2. Which pollen was most popular among the bees?
3. Which day was the best day for harvesting? Which one was the worst? (A chart or table that shows total sugar per day would be great.)
4. Which bee was most effective (**udarnik**)? Which one was the least? Effectiveness is measured as average sugar harvested per each working day. Getting a table with each bee relative effectiveness is a plus.

# Rules

* Use your favorite development platform.
* Quality first. Make it smart and well organized.
* Solve any number of tasks (but the more the better).
* Create models for the input data first.
* Use any preferred storage (memory-based is OK).
* Unit tested (queen likes to have the top code).

# Implementation note

1. In order to print the results please run the Honeybees file. The output is answer-friendly ;) 
2. In provided data there is always exactly one "max", "min", "most", "least", etc. answer for the tasks above. Nevertheless the methods return sequences as there might be a few entities with the same value, when different data is provided.
3. For more robust solution (if we are not sure about the input) there could be also validator for input data.

# Output

```
1. Which pollen contributed the most sugar in total?
	List((Pollen(1,Canola,10.0),43367.0))

2. Which pollen was most popular among the bees?
	List((Pollen(1,Canola,10.0),182))

3. Which day was the best day for harvesting? Which one was the worst? (A chart or table that shows total sugar per day would be great.
	Most efficient day: List((2013-05-26,2055.5))
	Least efficient day: List((2013-06-30,133.9))
	Sugar by day: 
		2013-04-01	647.2
		2013-04-02	589.9
		2013-04-03	759.3
		2013-04-04	522.1
		2013-04-05	791.4
		2013-04-06	658.6
		2013-04-07	478.1
		2013-04-08	504.4
		2013-04-09	514.5
		2013-04-10	421.3
		2013-04-11	989.8
		2013-04-12	1377.2
		2013-04-13	856.3
		2013-04-14	819.0
		2013-04-15	1154.2
		2013-04-16	610.6
		2013-04-17	959.0
		2013-04-18	1483.7
		2013-04-19	1034.6
		2013-04-20	507.4
		2013-04-21	847.2
		2013-04-22	1369.4
		2013-04-23	1031.8
		2013-04-24	1464.9
		2013-04-25	1734.3
		2013-04-26	990.2
		2013-04-27	728.8
		2013-04-28	1060.8
		2013-04-29	1160.0
		2013-04-30	491.7
		2013-05-01	1127.8
		2013-05-02	1112.6
		2013-05-03	1293.9
		2013-05-04	904.0
		2013-05-05	1213.6
		2013-05-06	1301.3
		2013-05-07	1363.5
		2013-05-08	1412.4
		2013-05-09	1113.4
		2013-05-10	770.9
		2013-05-11	775.6
		2013-05-12	1515.4
		2013-05-13	883.6
		2013-05-14	1208.9
		2013-05-15	694.2
		2013-05-16	1783.3
		2013-05-17	981.0
		2013-05-18	1031.6
		2013-05-19	1071.4
		2013-05-20	1693.2
		2013-05-21	1804.0
		2013-05-22	1058.2
		2013-05-23	1035.8
		2013-05-24	1433.2
		2013-05-25	1046.4
		2013-05-26	2055.5
		2013-05-27	1200.9
		2013-05-28	1048.5
		2013-05-29	1442.5
		2013-05-30	898.9
		2013-05-31	1295.9
		2013-06-01	680.9
		2013-06-02	1329.8
		2013-06-03	989.7
		2013-06-04	1110.0
		2013-06-05	656.0
		2013-06-06	1986.3
		2013-06-07	675.3
		2013-06-08	1054.6
		2013-06-09	950.6
		2013-06-10	1053.8
		2013-06-11	1226.8
		2013-06-12	1314.5
		2013-06-13	1121.3
		2013-06-14	1120.0
		2013-06-15	765.6
		2013-06-16	642.3
		2013-06-17	939.0
		2013-06-18	599.8
		2013-06-19	1186.8
		2013-06-20	943.1
		2013-06-21	597.0
		2013-06-22	996.0
		2013-06-23	413.4
		2013-06-24	1288.4
		2013-06-25	756.8
		2013-06-26	775.0
		2013-06-27	613.3
		2013-06-28	1268.4
		2013-06-29	1086.6
		2013-06-30	133.9

4. Which bee was most effective (**udarnik**)? Which one was the least? Effectiveness is measured as average sugar harvested per each working day. Getting a table with each bee relative effectiveness is a plus.
	Most effective bee: List((2,219.10923076923078))
	Least effective bee: List((11,53.675000000000004))
	Bees effectiveness (beeId, averageDailySugar): 
		1	85.2
		2	219.1
		3	105.7
		4	90.1
		5	93.5
		6	129.3
		7	57.3
		8	172.3
		9	119.2
		10	72.8
		11	53.7
		12	121.3
		13	97.9
		14	152.9
		15	204.3
```
